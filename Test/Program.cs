﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlySharp;

namespace Test
{
    class Program
    {
        static List<double> xVals = new List<double>();
        static List<double> yVals = new List<double>();
        static List<double> zVals = new List<double>();
        static List<double> rVals = new List<double>();
        static List<double> gVals = new List<double>();
        static List<double> bVals = new List<double>();
        static List<double> aVals = new List<double>();
        static List<List<double>> indVals = new List<List<double>>();

        static void Main(string[] args)
        {
            Console.WriteLine("Testing Ply Sharp Application...");

            ePlyStatus status;

            Console.WriteLine("Loading file");
            PlyHandler iply = new PlyHandler("Wy777a_DWT.ply");
            status = iply.ReadHeader();
            if (status != ePlyStatus.OK)
                Console.WriteLine("Could not read header, error - " + status.ToString());
            iply.SetDataReadCallback("vertex", "x", DataReadCallback);
            iply.SetDataReadCallback("vertex", "y", DataReadCallback);
            iply.SetDataReadCallback("vertex", "z", DataReadCallback);
            iply.SetDataReadCallback("vertex", "red", DataReadCallback);
            iply.SetDataReadCallback("vertex", "green", DataReadCallback);
            iply.SetDataReadCallback("vertex", "blue", DataReadCallback);
            iply.SetDataReadCallback("vertex", "alpha", DataReadCallback);
            iply.SetDataReadCallback("face", "vertex_indices", DataReadCallback);
            status = iply.ReadData();
            if (status != ePlyStatus.OK)
                Console.WriteLine("Could not read data, error - " + status.ToString());

            int vertexNum = iply.GetElement("vertex").Number;
            int faceNum = iply.GetElement("face").Number;

            Console.WriteLine("Writing file");
            PlyHandler oply = new PlyHandler("Wy777a_DWT_Test.ply");
            oply.SetFormat(new PlyFormat(ePlyDataFormat.ASCII, "1.0"));
            oply.AddComment("Created by PlySharp");
            oply.AddElement("vertex", vertexNum);
            oply.AddSingleProperty("x", ePlyDataType.FLOAT);
            oply.AddSingleProperty("y", ePlyDataType.FLOAT);
            oply.AddSingleProperty("z", ePlyDataType.FLOAT);
            oply.AddSingleProperty("red", ePlyDataType.UCHAR);
            oply.AddSingleProperty("green", ePlyDataType.UCHAR);
            oply.AddSingleProperty("blue", ePlyDataType.UCHAR);
            oply.AddSingleProperty("alpha", ePlyDataType.UCHAR);
            oply.AddElement("face", faceNum);
            oply.AddListProperty("vertex_indices", ePlyDataType.INT, ePlyDataType.UCHAR);
            oply.WriteHeader();

            status = oply.BeginWritingData();
            if (status != ePlyStatus.OK)
                Console.WriteLine("Could not begin writing data, error - " + status.ToString());
            for (int i = 0; i < vertexNum; i++)
            {
                status = oply.WriteDataValue(xVals[i]);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
                status = oply.WriteDataValue(yVals[i]);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
                status = oply.WriteDataValue(zVals[i]);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
                status = oply.WriteDataValue(255.0d /*rVals[i]*/);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
                status = oply.WriteDataValue(0.0d /*gVals[i]*/);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
                status = oply.WriteDataValue(0.0d /*bVals[i]*/);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
                status = oply.WriteDataValue(255.0d /*aVals[i]*/);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());
            }
            for (int i = 0; i < faceNum; i++)
            {
                status = oply.WriteDataValue(3.0d);
                if (status != ePlyStatus.OK)
                    Console.WriteLine("Could not write data value, error - " + status.ToString());

                List<double> inds = indVals[i];
                foreach (double idx in inds)
                {
                    status = oply.WriteDataValue(idx);
                    if (status != ePlyStatus.OK)
                        Console.WriteLine("Could not write data value, error - " + status.ToString());
                }
            }
            oply.EndWritingData();
            
            Console.WriteLine("Testing finished, press any key to exit...");
            Console.ReadKey();
        }

        static void DataReadCallback(PlyDataReadObject dro)
        {
            switch (dro.ElementName)
            {
                case "vertex":
                    {
                        switch (dro.PropertyName)
                        {
                            case "x":
                                xVals.Add(dro.Value);
                                break;
                            case "y":
                                yVals.Add(dro.Value);
                                break;
                            case "z":
                                zVals.Add(dro.Value);
                                break;
                            case "red":
                                rVals.Add(dro.Value);
                                break;
                            case "green":
                                gVals.Add(dro.Value);
                                break;
                            case "blue":
                                bVals.Add(dro.Value);
                                break;
                            case "alpha":
                                aVals.Add(dro.Value);
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case "face":
                    {
                        switch (dro.PropertyName)
                        {
                            case "vertex_indices":
                                if (dro.ListIndex == 0)
                                    indVals.Add(new List<double>());
                                indVals.Last().Add(dro.Value);
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                default:
                    break;
            }
        }
    }
}
