﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    class PlyDataReadTrigger
    {
        /* -------- Properties -------- */
        public string ElementName { get; private set; }
        public string PropertyName { get; private set; }
        public PlyHandler.DataReadCallbackMethod CallbackHandler { get; private set; }

        /* -------- Constructors -------- */
        public PlyDataReadTrigger(string inElementName, string inPropertyName, PlyHandler.DataReadCallbackMethod inCallbackHandler)
        {
            ElementName = inElementName;
            PropertyName = inPropertyName;
            CallbackHandler = inCallbackHandler;
        }
    }
}
