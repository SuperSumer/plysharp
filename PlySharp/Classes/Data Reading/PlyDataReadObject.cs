﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    public struct PlyDataReadObject
    {
        /* -------- Public Fields -------- */
        public string ElementName;
        public int ElementIndex;
        public string PropertyName;
        public int ListIndex;
        public double Value;
    }
}
