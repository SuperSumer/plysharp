﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlySharp
{
    class PlyHeader
    {
        /* -------- Properties ------- */
        public PlyFormat Format { get; set; }
        public List<PlyComment> Comments { get; private set; }
        public List<PlyElement> Elements { get; private set; }

        /* -------- Constructors -------- */
        public PlyHeader()
        {
            Format = new PlyFormat();
            Comments = new List<PlyComment>();
            Elements = new List<PlyElement>();
        }

        /* -------- Public Methods -------- */
        public void ParseLineParts(string[] parts)
        {
            // Skip blank lines
            if (parts.Length == 0)
                return;

            string firstPart = parts[0];

            if (firstPart == PlyConstants.PlyFormatString)
            {
                Format.ParseLineParts(parts);
            }
            else if (firstPart == PlyConstants.PlyCommentString)
            {
                if (parts.Length >= 2)
                    Comments.Add(new PlyComment(parts));
            }
            else if (firstPart == PlyConstants.PlyElementString)
            {
                PlyElement element = PlyElement.CreateFromLineParts(parts);
                if (element != null)
                    Elements.Add(element);
            }
            else if (firstPart == PlyConstants.PlyPropertyString)
            {
                if (Elements.Count > 0)
                {
                    PlyProperty property = PlyProperty.CreateFromLineParts(parts);
                    if (property != null)
                        Elements.Last().Properties.Add(property);
                }
            }
        }

        public List<string> GetLines()
        {
            List<string> lines = new List<string>();

            lines.Add(Format.GetLine());

            foreach (PlyComment c in Comments)
                lines.Add(c.GetLine());

            foreach (PlyElement e in Elements)
                lines.AddRange(e.GetLines());

            return lines;
        }
    }
}
