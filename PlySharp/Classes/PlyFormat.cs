﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    /* -------- Enumerations -------- */
    public enum ePlyDataFormat
    {
        UNKNOWN,
        ASCII,
        BINARY_LITTLE_ENDIAN,
        BINARY_BIG_ENDIAN
    }

    public class PlyFormat
    {
        /* -------- Public Fields -------- */
        public ePlyDataFormat DataFormat;
        public string Version;

        /* -------- Constructors -------- */
        public PlyFormat()
        {
            DataFormat = ePlyDataFormat.UNKNOWN;
            Version = "";
        }
        public PlyFormat(ePlyDataFormat inDataFormat, string inVersion)
        {
            DataFormat = inDataFormat;
            Version = inVersion;
        }

        /* -------- Public Methods -------- */
        public void ParseLineParts(string[] parts)
        {
            if (parts.Length >= 2)
                DataFormat = ParseDataFormat(parts[1]);
            if (parts.Length >= 3)
                Version = parts[2];
        }

        public string GetLine()
        {
            return "format " + GetDataFormatString(DataFormat) + " " + Version;
        }

        public bool EquivalentTo(PlyFormat format)
        {
            return (format.DataFormat == DataFormat && format.Version == Version);
        }

        /* -------- Private Methods -------- */
        private ePlyDataFormat ParseDataFormat(string s)
        {
            switch (s)
            {
                case "ascii":
                    return ePlyDataFormat.ASCII;
                case "binary_little_endian":
                    return ePlyDataFormat.BINARY_LITTLE_ENDIAN;
                case "binary_big_endian":
                    return ePlyDataFormat.BINARY_BIG_ENDIAN;
                default:
                    return ePlyDataFormat.UNKNOWN;
            }
        }

        private string GetDataFormatString(ePlyDataFormat dataFormat)
        {
            return dataFormat.ToString().ToLower();
        }
    }
}
