﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    public class PlyElement
    {
        /* -------- Properties -------- */
        public string Name { get; private set; }
        public int Number { get; private set; }
        public List<PlyProperty> Properties { get; private set; }

        /* -------- Constructors -------- */
        public PlyElement(string inName, int inNumber)
        {
            Name = inName;
            Number = inNumber;

            Properties = new List<PlyProperty>();
        }

        /* -------- Public Methods -------- */
        public List<string> GetLines()
        {
            List<string> lines = new List<string>();

            lines.Add("element " + Name + " " + Number.ToString());

            foreach (PlyProperty p in Properties)
                lines.Add(p.GetLine());

            return lines;
        }

        /* -------- Static Methods -------- */
        public static PlyElement CreateFromLineParts(string[] parts)
        {
            if (parts.Length < 3)
                return null;

            string name = parts[1];
            int number;
            if (!int.TryParse(parts[2], out number))
                return null;

            return new PlyElement(name, number);
        }
    }
}
