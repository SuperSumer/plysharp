﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlySharp
{
    struct PlyComment
    {
        /* -------- Public Fields ------- */
        public string Comment;

        /* -------- Constructors -------- */
        public PlyComment(string inComment)
        {
            Comment = inComment;
        }
        public PlyComment(string[] inParts)
        {
            Comment = "";

            for (int i = 1; i < inParts.Length; i++)
            {
                Comment += inParts[i];
                if (i < inParts.Length - 1)
                    Comment += " ";
            }
        }

        /* -------- Public Methods -------- */
        public string GetLine()
        {
            return "comment " + Comment;
        }
    }
}
