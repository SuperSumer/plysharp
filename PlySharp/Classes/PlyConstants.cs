﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    static class PlyConstants
    {
        /* -------- Properties -------- */
        public static string PlyHeaderStartString { get { return "ply"; } }
        public static string PlyHeaderEndString { get { return "end_header"; } }
        public static string PlyListString { get { return "list"; } }
        public static string PlyFormatString { get { return "format"; } }
        public static string PlyCommentString { get { return "comment"; } }
        public static string PlyElementString { get { return "element"; } }
        public static string PlyPropertyString { get { return "property"; } }
        public static System.Text.Encoding PlyTextEncoding { get { return System.Text.Encoding.ASCII; } }

        /* -------- Public Fields -------- */
        public static PlyFormat[] SupportedDataFormats = new PlyFormat[]
        {
            new PlyFormat(ePlyDataFormat.ASCII, "1.0")
        };

        /* -------- Public Methods -------- */
        public static bool FormatIsSupported(PlyFormat format)
        {
            foreach (PlyFormat f in SupportedDataFormats)
                if (f.EquivalentTo(format))
                    return true;

            return false;
        }
    }
}
