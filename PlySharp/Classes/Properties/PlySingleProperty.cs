﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    public class PlySingleProperty : PlyProperty
    {
        /* -------- Constructors -------- */
        public PlySingleProperty(string inName, ePlyDataType inDataType)
            : base(inName, inDataType)
        {
        }

        /* -------- Public Methods -------- */
        public override string GetLine()
        {
            return "property " + GetDataTypeString(DataType) + " " + Name;
        }

        /* -------- Static Methods -------- */
        public new static PlySingleProperty CreateFromLineParts(string[] parts)
        {
            if (parts.Length < 3)
                return null;

            ePlyDataType dataType = ParseDataType(parts[1]);
            string name = parts[2];

            return new PlySingleProperty(name, dataType);
        }
    }
}
