﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    /* ------- Enumerations ------- */
    public enum ePlyDataType
    {
        // First data type set
        CHAR,
        UCHAR,
        SHORT,
        USHORT,
        INT,
        UINT,
        FLOAT,
        DOUBLE,
        // Second data type set
        INT8,
        UINT8,
        INT16,
        UINT16,
        INT32,
        UINT32,
        FLOAT32,
        FLOAT64
    }

    public abstract class PlyProperty
    {
        /* ------- Properties -------- */
        public string Name { get; private set; }
        public ePlyDataType DataType { get; private set; }

        /* -------- Constructors -------- */
        public PlyProperty(string inName, ePlyDataType inDataType)
        {
            Name = inName;
            DataType = inDataType;
        }

        /* -------- Public Methods -------- */
        public abstract string GetLine();

        /* -------- Static Methods -------- */
        public static PlyProperty CreateFromLineParts(string[] parts)
        {
            if (parts.Length < 2)
                return null;

            // Check if it's a list property, otherwise must be a single property
            if (parts[1] == PlyConstants.PlyListString)
                return PlyListProperty.CreateFromLineParts(parts);

            return PlySingleProperty.CreateFromLineParts(parts);
        }

        public static double ParseValue(string s, ePlyDataType dataType)
        {
            switch (dataType)
            {
                case ePlyDataType.CHAR:
                    {
                        sbyte val = 0;
                        sbyte.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.UCHAR:
                    {
                        byte val = 0;
                        byte.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.SHORT:
                    {
                        short val = 0;
                        short.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.USHORT:
                    {
                        ushort val = 0;
                        ushort.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.INT:
                    {
                        int val = 0;
                        int.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.UINT:
                    {
                        uint val = 0;
                        uint.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.FLOAT:
                    {
                        float val = 0.0f;
                        float.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.DOUBLE:
                    {
                        double val = 0.0d;
                        double.TryParse(s, out val);
                        return val;
                    }
                case ePlyDataType.INT8:
                    {
                        sbyte val = 0;
                        sbyte.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.UINT8:
                    {
                        byte val = 0;
                        byte.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.INT16:
                    {
                        short val = 0;
                        short.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.UINT16:
                    {
                        ushort val = 0;
                        ushort.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.INT32:
                    {
                        int val = 0;
                        int.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.UINT32:
                    {
                        uint val = 0;
                        uint.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.FLOAT32:
                    {
                        float val = 0.0f;
                        float.TryParse(s, out val);
                        return (double)val;
                    }
                case ePlyDataType.FLOAT64:
                    {
                        double val = 0.0d;
                        double.TryParse(s, out val);
                        return val;
                    }
                default:
                    return 0.0d;
            }
        }
        public static string WriteValue(double val, ePlyDataType dataType)
        {
            switch (dataType)
            {
                case ePlyDataType.CHAR:
                    return ((sbyte)val).ToString();
                case ePlyDataType.UCHAR:
                    return ((byte)val).ToString();
                case ePlyDataType.SHORT:
                    return ((short)val).ToString();
                case ePlyDataType.USHORT:
                    return ((ushort)val).ToString();
                case ePlyDataType.INT:
                    return ((int)val).ToString();
                case ePlyDataType.UINT:
                    return ((uint)val).ToString();
                case ePlyDataType.FLOAT:
                    return ((float)val).ToString();
                case ePlyDataType.DOUBLE:
                    return val.ToString();
                case ePlyDataType.INT8:
                    return ((sbyte)val).ToString();
                case ePlyDataType.UINT8:
                    return ((byte)val).ToString();
                case ePlyDataType.INT16:
                    return ((short)val).ToString();
                case ePlyDataType.UINT16:
                    return ((ushort)val).ToString();
                case ePlyDataType.INT32:
                    return ((int)val).ToString();
                case ePlyDataType.UINT32:
                    return ((uint)val).ToString();
                case ePlyDataType.FLOAT32:
                    return ((float)val).ToString();
                case ePlyDataType.FLOAT64:
                    return val.ToString();
                default:
                    return "";
            }
        }

        protected static ePlyDataType ParseDataType(string s)
        {
            ePlyDataType dataType;
            if (!Enum.TryParse<ePlyDataType>(s, true, out dataType))
                return ePlyDataType.DOUBLE; // Default to double

            return dataType;
        }

        protected static string GetDataTypeString(ePlyDataType dataType)
        {
            return dataType.ToString().ToLower();
        }
    }
}
