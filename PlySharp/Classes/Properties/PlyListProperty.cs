﻿using System;
using System.Collections.Generic;

namespace PlySharp
{
    public class PlyListProperty : PlyProperty
    {
        /* ------- Properties -------- */
        public ePlyDataType CountDataType { get; private set; }

        /* -------- Constructors -------- */
        public PlyListProperty(string inName, ePlyDataType inDataType, ePlyDataType inListDataType)
            : base(inName, inDataType)
        {
            CountDataType = inListDataType;
        }

        /* -------- Public Methods -------- */
        public override string GetLine()
        {
            return "property list " + GetDataTypeString(CountDataType) + " " + GetDataTypeString(DataType) + " " + Name;
        }

        /* -------- Static Methods -------- */
        public new static PlyListProperty CreateFromLineParts(string[] parts)
        {
            if (parts.Length < 5)
                return null;

            ePlyDataType listDataType = ParseDataType(parts[2]);
            ePlyDataType dataType = ParseDataType(parts[3]);
            string name = parts[4];

            return new PlyListProperty(name, dataType, listDataType);
        }
    }
}
