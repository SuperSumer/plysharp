﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace PlySharp
{
    /* -------- Enumerations -------- */
    public enum ePlyStatus
    {
        OK,
        ERROR_INVALID_FILE_CONTENTS,
        ERROR_UNEXPECTED_END_OF_FILE,
        ERROR_UNHANDLED_EXCEPTION,
        ERROR_INVALID_DATA_FORMAT,
        ERROR_UNSUPPORTED_DATA_FORMAT,
        ERROR_DATA_OVERFLOW
    }

    public class PlyHandler
    {
        /* -------- Delegates -------- */
        public delegate void DataReadCallbackMethod(PlyDataReadObject dro);

        /* -------- Properties -------- */
        public string FilePath { get; private set; }
        
        /* -------- Private Fields -------- */
        private PlyHeader header;
        private List<PlyDataReadTrigger> readTriggers;
        // Data counters
        private int elementIndex = 0;
        private int elementCount = 0;
        private int propertyIndex = 0;
        private int listIndex = 0;
        private int listLength = 0;
        private bool isWalkingList = false;
        // Data Writer
        private StreamWriter dataWriter;

        /* -------- Constructors -------- */
        public PlyHandler(string inFilePath)
        {
            FilePath = inFilePath;

            header = new PlyHeader();
            readTriggers = new List<PlyDataReadTrigger>();
        }

        /* ------- Public Methods --------*/
        // Header
        public ePlyStatus ReadHeader()
        {
            try
            {
                using (StreamReader sr = new StreamReader(FilePath, PlyConstants.PlyTextEncoding))
                {
                    string s = sr.ReadLine();
                    int line = 1;
                    while (s != null)
                    {
                        string[] parts = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        // Include header start and end as special cases
                        if (line == 1)
                        {
                            // First line of file must be 'ply'
                            if (parts.Length == 0 || parts[0] != PlyConstants.PlyHeaderStartString)
                                return ePlyStatus.ERROR_INVALID_FILE_CONTENTS;
                        }
                        else if (parts.Length > 0 && parts[0] == PlyConstants.PlyHeaderEndString)
                        {
                            // End of ply header
                            return ePlyStatus.OK;
                        }
                        else
                            header.ParseLineParts(parts);

                        s = sr.ReadLine();
                        line++;
                    }

                    // Reached end of file before end_header found, return error
                    return ePlyStatus.ERROR_UNEXPECTED_END_OF_FILE;
                }
            }
            catch (Exception)
            {
                return ePlyStatus.ERROR_UNHANDLED_EXCEPTION;
            }
        }
        public ePlyStatus WriteHeader()
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(FilePath, false, PlyConstants.PlyTextEncoding))
                {
                    sw.WriteLine("ply");
                    foreach (string l in header.GetLines())
                        sw.WriteLine(l);
                    sw.WriteLine("end_header");
                }

                return ePlyStatus.OK;
            }
            catch (Exception)
            {
                return ePlyStatus.ERROR_UNHANDLED_EXCEPTION;
            }
        }
        public PlyFormat GetFormat()
        {
            return header.Format;
        }
        public void SetFormat(PlyFormat inFormat)
        {
            header.Format = inFormat;
        }
        public void AddComment(string comment)
        {
            header.Comments.Add(new PlyComment(comment));
        }
        public string[] GetComments()
        {
            string[] comments = new string[header.Comments.Count];

            for (int i = 0; i < header.Comments.Count; i++)
                comments[i] = header.Comments[i].Comment;
            
            return comments;
        }

        // Elements and properties
        public PlyElement GetElement(string name)
        {
            return header.Elements.Find(x => x.Name == name);
        }
        public void AddElement(string inName, int inNumber)
        {
            header.Elements.Add(new PlyElement(inName, inNumber));
        }
        public void AddSingleProperty(string inName, ePlyDataType inDataType)
        {
            if (header.Elements.Count == 0)
                return;

            PlyElement element = header.Elements.Last();
            element.Properties.Add(new PlySingleProperty(inName, inDataType));
        }
        public void AddListProperty(string inName, ePlyDataType inDataType, ePlyDataType inListDataType)
        {
            if (header.Elements.Count == 0)
                return;

            PlyElement element = header.Elements.Last();
            element.Properties.Add(new PlyListProperty(inName, inDataType, inListDataType));
        }

        // Data
        public void SetDataReadCallback(string inElementName, string inPropertyName, DataReadCallbackMethod inCallbackMethod)
        {
            readTriggers.Add(new PlyDataReadTrigger(inElementName, inPropertyName, inCallbackMethod));
        }
        public ePlyStatus ReadData()
        {
            if (header.Format.DataFormat == ePlyDataFormat.UNKNOWN)
                return ePlyStatus.ERROR_INVALID_DATA_FORMAT;

            if (!PlyConstants.FormatIsSupported(header.Format))
                return ePlyStatus.ERROR_UNSUPPORTED_DATA_FORMAT;
            
            ResetDataCounters();

            bool reachedData = false;
            using (StreamReader sr = new StreamReader(FilePath, PlyConstants.PlyTextEncoding))
            {
                string line = sr.ReadLine();
                while (line != null && elementIndex < header.Elements.Count)
                {
                    if (reachedData)
                        ParseDataLine(line);
                    else
                    {
                        if (line == PlyConstants.PlyHeaderEndString)
                            reachedData = true;
                    }

                    line = sr.ReadLine();
                }
            }

            return ePlyStatus.OK;
        }
        public ePlyStatus BeginWritingData()
        {
            ResetDataCounters();

            try
            {
                dataWriter = new StreamWriter(FilePath, true, PlyConstants.PlyTextEncoding);
                return ePlyStatus.OK;
            }
            catch (Exception)
            {
                return ePlyStatus.ERROR_UNHANDLED_EXCEPTION;
            }
        }
        public ePlyStatus WriteDataValue(double val)
        {
            if (header.Format.DataFormat == ePlyDataFormat.ASCII && header.Format.Version == "1.0")
                return WriteDataValue_ASCII_1_0(val);

            return ePlyStatus.ERROR_UNSUPPORTED_DATA_FORMAT;
        }
        public void EndWritingData()
        {
            dataWriter.Close();
            dataWriter.Dispose();
        }

        /* -------- Private Methods -------- */
        private void ResetDataCounters()
        {
            elementIndex = 0;
            elementCount = 0;
            propertyIndex = 0;
            listIndex = 0;
            listLength = 0;
        }

        private void ParseDataLine(string line)
        {
            if (header.Format.DataFormat == ePlyDataFormat.ASCII && header.Format.Version == "1.0")
                ParseDataLine_ASCII_1_0(line);
        }
        private void ParseDataLine_ASCII_1_0(string line)
        {
            string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            PlyElement element = header.Elements[elementIndex];

            for (int i = 0; i < parts.Length; i++)
            {
                string part = parts[i];
                PlyProperty property = element.Properties[propertyIndex];

                if (property is PlySingleProperty)
                {
                    double val = PlyProperty.ParseValue(part, property.DataType);

                    PlyDataReadObject dro = new PlyDataReadObject()
                    {
                        ElementName = element.Name,
                        ElementIndex = elementCount,
                        PropertyName = property.Name,
                        ListIndex = 0,
                        Value = val
                    };
                    FireCallbacks(dro);

                    if (IteratePropertyCounter())
                        break;
                }

                PlyListProperty listProperty = property as PlyListProperty;
                if (listProperty != null)
                {
                    if (!isWalkingList)
                    {
                        isWalkingList = true;
                        listIndex = 0;
                        listLength = (int)PlyProperty.ParseValue(part, listProperty.CountDataType);
                        continue;
                    }

                    double val = PlyProperty.ParseValue(part, property.DataType);
                    PlyDataReadObject dro = new PlyDataReadObject()
                    {
                        ElementName = element.Name,
                        ElementIndex = elementCount,
                        PropertyName = property.Name,
                        ListIndex = listIndex,
                        Value = val
                    };
                    FireCallbacks(dro);

                    if (IterateListCounter())
                        if (IteratePropertyCounter())
                            break;
                }
            }

            IterateElementCounter();
        }

        private ePlyStatus WriteDataValue_ASCII_1_0(double val)
        {
            if (elementIndex >= header.Elements.Count)
                return ePlyStatus.ERROR_DATA_OVERFLOW;

            PlyElement element = header.Elements[elementIndex];
            PlyProperty property = element.Properties[propertyIndex];

            if (propertyIndex > 0)
                dataWriter.Write(' ');

            // List property
            PlyListProperty listProperty = property as PlyListProperty;
            if (listProperty != null)
            {
                if (!isWalkingList)
                {
                    dataWriter.Write(PlyProperty.WriteValue(val, listProperty.CountDataType));
                    listLength = (int)val;
                    isWalkingList = true;
                    return ePlyStatus.OK;
                }

                dataWriter.Write(' ');

                dataWriter.Write(PlyProperty.WriteValue(val, listProperty.DataType));

                if (IterateListCounter())
                {
                    if (IteratePropertyCounter())
                    {
                        dataWriter.WriteLine();
                        IterateElementCounter();
                    }
                }

                return ePlyStatus.OK;
            }

            // Single property
            dataWriter.Write(PlyProperty.WriteValue(val, property.DataType));

            if (IteratePropertyCounter())
            {
                dataWriter.WriteLine();
                IterateElementCounter();
            }

            return ePlyStatus.OK;
        }

        private bool IterateElementCounter()
        {
            elementCount++;
            if (elementCount >= header.Elements[elementIndex].Number)
            {
                elementIndex++;
                elementCount = 0;
                return true;
            }

            return false;
        }
        private bool IteratePropertyCounter()
        {
            propertyIndex++;
            if (propertyIndex >= header.Elements[elementIndex].Properties.Count)
            {
                propertyIndex = 0;
                return true;
            }

            return false;
        }
        private bool IterateListCounter()
        {
            listIndex++;
            if (listIndex >= listLength)
            {
                isWalkingList = false;
                listIndex = 0;
                return true;
            }

            return false;
        }

        private void FireCallbacks(PlyDataReadObject dro)
        {
            foreach (PlyDataReadTrigger drt in readTriggers)
            {
                if (drt.ElementName != dro.ElementName)
                    continue;
                if (drt.PropertyName != dro.PropertyName)
                    continue;

                drt.CallbackHandler(dro);
            }
        }
    }
}
